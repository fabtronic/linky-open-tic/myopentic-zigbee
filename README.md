# myOpenTIC-ZigBee

Projet d'un module de lecture et de transmission de la sortie Télé-Information Client (TIC) autour de module ZigBee.

## Liens Internet

### Recherche hardware pour notre projet

CC2652P : voir module [E72-2G4M20S1E](https://www.ebyte.com/en/product-view-news.html?id=1002)

### Solutions Open-Source

[ZLinky_TIC](https://lixee.fr/produits/37-73-zigate-usb-ttl-3770014375148.html) - <https://github.com/fairecasoimeme/Zlinky_TIC>

### Solutions Propriétaires
